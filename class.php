<?php

class User 
{
    private $id;
    private $name;
    private $password;
    private $email;
    private $status; 
    private $created;
    private $updated;
    
    /**
     * 
     * @param string $name
     * @param string $email
     */
    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }
    
    protected function printInfo()
    {
        var_dump([
            'name' => $this->name, 
            'email' => $this->email
        ]);
    }
    
    public function login() 
    {
        echo 'Logged In..';
    }
    
    public function register() 
    {
        echo 'Registered..';
    }
}

class Admin extends User 
{
    public function printMessage() 
    {
        $this->printInfo();
    }
}

//$user = new User('Jamiu', 'mojolagm@myumanitoba.ca');
//$user->printInfo();

$admin = new Admin('Jamiu', 'mojolagm@myumanitoba.ca');
$admin->printMessage();