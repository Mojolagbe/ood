const path = require('path');

module.exports = {
    entry: './scripts/script.js',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'build'),
    },
};