<?php 

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';
    
if (isset($_GET['submit'])) {
     $subject = $_POST["subject"] ?? '';
     $email = $_POST["email"] ?? '';
     $message = $_POST["message"] ?? '';
     /*mail($email, $subject, $message);
     
    //Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    

    //Create an instance; passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host = 'smtp.example.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth = true;                                   //Enable SMTP authentication
        $mail->Username = 'user@example.com';                     //SMTP username
        $mail->Password = 'secret';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        //Recipients
        $mail->setFrom('from@example.com', 'Mailer');
        $mail->addAddress('joe@example.net', 'Joe User');     //Add a recipient
        $mail->addAddress('ellen@example.com');               //Name is optional
        $mail->addReplyTo('info@example.com', 'Information');
        $mail->addCC('cc@example.com');
        $mail->addBCC('bcc@example.com');

        //Attachments
        $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
        $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name
        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    } */
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Page Title</title>
        <!-- external stylesheets -->
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/another.css" rel="stylesheet" />
        <style>
            /* internal style */
        </style>
    </head>
    <body>
        <h1 id="heading">This is a Heading</h1>
        <p class="blue" style="color: pink">This is a paragraph.</p> <!-- inline style... avoid inline style -->
        <p>I don't want blue color here</p>
        <p class="blue">I am blue color</p>
        <button id="alertbtn" onclick="alert('You clicked a button!');">Show Message</button>
        <button id="alertbtn2">Click Me!</button>
        
        <form action="?submit" method="POST">
            <div>
                <label for="subject">Subject:</label>
                <input type="text" id="subject" name="subject" value="">
            </div><!-- comment -->
            <div>
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" value="">
            </div><!-- comment -->
            <div>
                <label for="message">Message:</label>
                <textarea id="message" name="message"></textarea>
            </div><!-- comment -->
            <button type="submit">Send Email</button>
        </form>
        
        
        <script src="build/index.js"></script>
    </body>
</html>